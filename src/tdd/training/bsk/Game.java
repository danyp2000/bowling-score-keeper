package tdd.training.bsk;

public class Game {
	
	private int framesNumber;
	private int firstBonus;
	private int secondBonus;
	private Frame[] frames;
	 

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new Frame[10];
		framesNumber = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frames[getFramesNumber()] = frame;
		framesNumberUp();
	}

	

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		firstBonus = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		secondBonus = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		
		for (int x = 0; x < getFramesNumber(); x++) {
			
			if (frames[x].isSpare()) {
				
				if (x == getFramesNumber() - 1) {
					frames[x].setBonus(getFirstBonusThrow());
				}
				
				else {
					frames[x].setBonus(frames[x+1].getFirstThrow());
				}
			}
			
			else if (frames[x].isStrike()) {
				
				if (x == getFramesNumber() - 1) {
					frames[x].setBonus(getFirstBonusThrow() + getSecondBonusThrow());
				}
				
				else if (x == getFramesNumber() - 2) {
					
					if(frames[x+1].isStrike()) {
						frames[x].setBonus(frames[x+1].getFirstThrow() + getFirstBonusThrow());
					}
				
					else {
						frames[x].setBonus(frames[x+1].getFirstThrow() + frames[x+1].getSecondThrow());
					}	
				}
				
				else {
					
					if (frames[x+1].isStrike()) {
						frames[x].setBonus(frames[x+1].getFirstThrow() + frames[x+2].getFirstThrow());
					}
					
					else {
						frames[x].setBonus(frames[x+1].getFirstThrow() + frames[x+1].getSecondThrow());
					}	
				}
			}
			score += frames[x].getScore();
		}
		
		return score;	
	}

	public int getFramesNumber() {
		return framesNumber;
	}
	
	private void framesNumberUp() {
		framesNumber++;
	}
}
