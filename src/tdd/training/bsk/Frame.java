package tdd.training.bsk;

public class Frame {
	
	private int[] frameThrows = new int[2];
	private int bonusPoints = 0;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		frameThrows[0] = firstThrow;
		frameThrows[1] = secondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return frameThrows[0];
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return frameThrows[1];
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		bonusPoints = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonusPoints;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		int basePoints = getFirstThrow() + getSecondThrow();
		if (isSpare() || isStrike()) {
			return basePoints + getBonus();
		}
		return basePoints;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		if(getFirstThrow() == 10) {
			return true;
		}
		
		return false;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		if (getFirstThrow() + getSecondThrow() == 10 && getFirstThrow() != 10) {
			return true;
		}
		return false;
	}
	
	public boolean equals(Frame frame) {
		return (getFirstThrow() == frame.getFirstThrow()) && 
			   (getSecondThrow() == frame.getSecondThrow());
	}

}
