package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.*;

public class GameTest {

	@Test
	public void framesShouldBe10() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,4));
		game.addFrame(new Frame(6,3));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(9,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(2,2));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(1,3));
		assertEquals(10, game.getFramesNumber());
	}

	@Test
	public void testGetFrameAt() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,4));
		game.addFrame(new Frame(6,3));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(9,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(2,2));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(1,3));
		Frame x = new Frame(4,6);
		assertTrue(x.equals(game.getFrameAt(5)));
	}
	
	@Test
	public void testCalculateScore() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,4));
		game.addFrame(new Frame(6,3));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(3,4));
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(5,3));
		assertEquals(78, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithSpareBonus() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,4));
		game.addFrame(new Frame(6,3));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(3,4));
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(5,3));
		assertEquals(83, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeBonus() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(6,3));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(3,4));
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(5,3));
		assertEquals(91, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeSpareBonus() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(6,4));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(3,2));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(5,3));
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleStrikeBonus() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleSparesBonus() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testSetGetFirstBonusThrow() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testCalculateScoreWithSpareAsLastFrame() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(7);
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testSetGetSecondBonusThrow() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setSecondBonusThrow(5);
		assertEquals(5, game.getSecondBonusThrow());
	}
	
	@Test
	public void testCalculateScoreWithStrikeAsLastFrame() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testCalculateBestScore() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300, game.calculateScore());
	}
}
