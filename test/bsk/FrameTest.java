package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testGetFirstThrow() throws BowlingException{
		Frame frame = new Frame(2, 5);
		assertEquals(frame.getFirstThrow(), 2);
	}
	
	@Test
	public void testGetSecondThrow() throws BowlingException{
		Frame frame = new Frame(3, 7);
		assertEquals(frame.getSecondThrow(), 7);
	}
	
	@Test
	public void testGetScore() throws BowlingException{
		Frame frame = new Frame(2, 8);
		assertEquals(frame.getScore(), 10);
	}
	
	@Test
	public void testSetGetBonus() throws BowlingException{
		Frame frame = new Frame(2, 8);
		frame.setBonus(6);
		assertEquals(frame.getBonus(), 6);
	}

	@Test
	public void shouldBeSpare() throws BowlingException{
		Frame frame = new Frame(4, 6);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testGetScoreWithSpareBonus() throws BowlingException{
		Frame frame = new Frame(5, 5);
		frame.setBonus(2);
		assertEquals(12, frame.getScore());
	}
	
	@Test
	public void shouldBeStrike() throws BowlingException{
		Frame frame = new Frame(10, 0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testGetScoreWithStrikeBonus() throws BowlingException{
		Frame frame = new Frame(10, 0);
		frame.setBonus(4+5);
		assertEquals(19, frame.getScore());
	}
}
